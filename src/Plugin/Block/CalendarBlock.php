<?php

namespace Drupal\brussels_calendar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Driver\mysql\Connection;

use Drupal\views\Views;

use Drupal\node\Entity\Node;

use Drupal\Core\Url; 

use Drupal\Core\Cache\Cache;


/**
 * Provides a 'CalendarBlock' block.
 *
 * @Block(
 *  id = "calendar_block",
 *  admin_label = @Translation("Brussels Calendar Block"),
 * )
 */
class CalendarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  
  
  private $customCacheTags=['brussels_calendar'];
  /**
   * Constructs a new CalendarBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $database
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view' => $this->t(''),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
  	
  	$viewsList=[];
  	
  	
  	foreach(Views::getAllViews() as $view){
  		$viewsList[$view->id()]=$view->label();
  	}
  	
  	
    $form['view'] = [
      '#type' => 'select',
      '#options' => $viewsList,
      '#title' => $this->t('View'),
      '#description' => $this->t('Enter the system name of the view from witch the list of nodes will be retrieved.'),
      '#default_value' => $this->configuration['view'],
      '#weight' => '0',
      '#required'=> true,
    ];
    
    $calendarId=$this->configuration['calendar_id'];
    if(empty($calendarId)){
    	$calendarId='brussels_calendar';
    }
    
    $form['calendar_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element ID'),
      '#description' => $this->t('Enter the ID of the calendar to be used.'),
      '#default_value' => $calendarId,
      '#weight' => '0',
      '#required'=> true,
    ];
    
    $timestapField=$this->configuration['timestamp_field'];
    if(empty($timestapField)){
    	$timestapField='created';
    }
    
    $form['timestamp_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp field'),
      '#description' => $this->t('Enter the field of content type used to get the UNIX timestamp value. This should be a datetime field..'),
      '#default_value' => $timestapField,
      '#weight' => '0',
      '#required'=> true,
    ];
    
    
    $javascriptFunction=$this->configuration['javascript_function'];
    $form['javascript_function'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Javascript function'),
      '#description' => $this->t('Enter a javascript function to be executed on event click. The function must be globally defined and will receive the node as associative array (id and eventName). The user will not be forwarded to the node page.'),
      '#default_value' => $javascriptFunction,
      '#weight' => '0',
      '#required'=> false,
    ];

    return $form;
  }


  public function blockValidate($form, FormStateInterface $form_state) {
  	
  	
  	$viewName=trim($form_state->getValue('view'));
  	$view = Views::getView($viewName);
  	if(empty($view)){
  		$form_state->setErrorByName('view', $this->t("View doesn't exist"));
  	}
  	
  	
  	//TODO validate field type.
  	if(!empty($view)){
  		$view->execute();
  		if(count($view->result)>0){
  			$node = Node::load($view->result[0]->nid);
  			
  			$timestampField=null;
			
			try{
	  			$timestampField=$node->get($form_state->getValue('timestamp_field'));
			}catch(\InvalidArgumentException $exception){
				$form_state->setErrorByName('timestamp_field', $exception->getMessage());
			}
			if(!empty($timestampField)){
				
				$timestapFieldValue=$timestampField->getValue();
				if(count($timestapFieldValue)==0){
					$form_state->setErrorByName('timestamp_field', 'Has no value (based on first item in view)');
				}else{
					if(!array_key_exists('value', $timestapFieldValue[0])){
						$form_state->setErrorByName('timestamp_field', 'Has no value field (based on first item in view)');
					}else{
						if(intval($timestapFieldValue[0]['value'])<=0){
							$form_state->setErrorByName('timestamp_field', 'Value invalid as a UNIX timestamp');
						}
					}
				}
				
				//$form_state->setErrorByName('timestamp_field', $timestampField->getFieldDefinition()->getType());
			}
			
			  	
  		}
  	}
  	
  	/*$timestampField=$node->get($form_state->getValue('timestamp_field'));
  	
  	*/
  	//->getFieldDefinition()->getType();
    
    
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view'] = trim($form_state->getValue('view'));
    $this->configuration['calendar_id'] = trim($form_state->getValue('calendar_id'));
    $this->configuration['timestamp_field'] = trim($form_state->getValue('timestamp_field'));
    $this->configuration['javascript_function'] = trim($form_state->getValue('javascript_function'));
    
    //Delete cache.
    \Drupal::service('cache_tags.invalidator')->invalidateTags($this->customCacheTags);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
  	$config = $this->getConfiguration();
  	
  	$viewName='';
  	if (!empty($config['view'])) {
      $viewName = $config['view'];
    }
    $build = [];
    
    $build['#theme']='brussels_calendar';
    
	$build['#attached']['library'][] = 'brussels_calendar/brussels_calendar';
	$build['#attached']['library'][] = 'brussels_calendar/momentjs';
	
	$build['#calendar_id']=$config['calendar_id'];
	
	$build['#settings']=json_encode([
		'no_events'=>$this->t("No events"),
	]);
	
	
	$javascriptFunction=$config['javascript_function'];
	if(empty($javascriptFunction)){
		$javascriptFunction='null';
	}
	
	$build['#javascript_function']=$javascriptFunction;

	$events=[];
	
	$view = Views::getView($viewName);
	
	//$viewResult = \Drupal::service('renderer')->renderRoot($view->render());
	
	$node_storage=\Drupal::entityManager()->getStorage('node');
	
	if(!empty($view)){
		$view->execute();
		
		foreach($view->result as $row){
			$node = Node::load($row->nid);
			
			$events[] = [
				'eventName'=>$node->getTitle(),
				'calendar'=>$this->t('Event'),
				'color'=>'default',
				'date'=>$node->get($config['timestamp_field'])->getValue()[0]['value'],//format("Y-m-d H:i:s"),
				'url'=> Url::fromRoute('entity.node.canonical', ['node' => $row->nid])->toString()
			];
		}
	}
	
	//print_r($nodes);
	$build['#events']=json_encode($events, true);
	
    return $build;
  }
  
	public function getCacheTags() {
		return Cache::mergeTags(parent::getCacheTags(), $this->customCacheTags);
	}

}
